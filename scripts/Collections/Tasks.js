define(['backbone', 'Models/Task'], function(Backbone, Task) {
  "use strict";
  var Tasks = Backbone.Collection.extend({
    model: Task
  });
  return Tasks;
});
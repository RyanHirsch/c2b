define(['backbone'], function(Backbone) {
  "use strict";
  var Task = Backbone.Model.extend({
    defaults: {
      title: "",
      priority: 3
    }
  });
  return Task;
});
require([ 'jquery', 'foundation/app']
, function($) {
  require(['jquery', 'Models/Task', 'Collections/Tasks', 'Views/Task', 'Views/Tasks', 'Views/AddTask']
  , function($, TaskModel, TaskCollection, TaskView, TasksView, AddTaskView) {
    window.App = {
      Models: {
        Task: TaskModel
      },
      Collections: {
        Tasks: TaskCollection
      },
      Views: {
        Task: TaskView,
        Tasks: TasksView,
        AddTask: AddTaskView
      }
    };

    var tasks = new App.Collections.Tasks([
      {
        title: "Hello"
      },
      {
        title: "World"
      },
      {
        title: "Test"
      },
      {
        title: "John"
      }
    ]);

    var tasksView = new App.Views.Tasks({
      collection: tasks
    });
    var addTaskView = new App.Views.AddTask({
      collection: tasks
    });

    $('#content').append(tasksView.render().el);
  });
});

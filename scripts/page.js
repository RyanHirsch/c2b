require(['jquery', 'backbone', 'Routers/Router']
, function($, Backbone, Router) {
  window.App = {
    Models: {
    },
    Collections: {
    },
    Views: {
    },
    Routers: {
    },
    Router: Router
  };

  new App.Router;
  Backbone.history.start();
});
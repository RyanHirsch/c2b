define(['backbone']
, function(Backbone) {
  var Router = Backbone.Router.extend({
    routes: {
      '': 'index',
      'show': 'show'
    },
    index: function() {
      console.log("hello from index");
    },
    show: function() {
      console.log("hello from show!!");
    }
  });
  return Router;
});
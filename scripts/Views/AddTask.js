define(['backbone', 'Models/Task']
, function(Backbone, Task) {
  "use strict";
  var AddTask = Backbone.View.extend({
    el: '#newTask',
    initialize: function() {
    },
    events: {
      "submit": "submit"
    },
    submit: function(e) {
      e.preventDefault();
      var taskTitle = $.trim($(e.currentTarget).find("input[type=text]").val());
      $(e.currentTarget).find("input[type=text]").val("");
      if(taskTitle) this.collection.add(new Task({title: taskTitle}));
    }
  });
  return AddTask;
});
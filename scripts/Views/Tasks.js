define(['jquery', 'Views/Task', 'Collections/Tasks']
, function($, TaskView, TasksCollection) {
  "use strict";
  var TaskCollectionView = Backbone.View.extend({
    tagName: 'div',
    className: 'to-do-items six columns',
    model: TasksCollection,
    initialize: function() {
      this.collection.on('add', this.addOne, this);
    },
    render: function() {
      this.collection.each(this.addOne, this);
      return this;
    },
    addOne: function(taskModel) {
      var taskView = new TaskView({model: taskModel});
      this.$el.append(taskView.render().el);
    }
  });
  return TaskCollectionView;  
});
  
define(['handlebars', 'backbone', 'Models/Task', 'text!template/Task.hbs', 'text!template/EditTask.hbs']
, function(Handlebars, Backbone, Task, TaskTemplate, EditTaskTemplate) {
  "use strict";
  var TaskView = Backbone.View.extend({
    tagName: 'div',
    className: 'row display',
    template: Handlebars.compile(TaskTemplate),
    editTemplate: Handlebars.compile(EditTaskTemplate),
    events: {
      'click .edit': 'editTask',
      'click .delete': 'destroyTask'
    },
    initialize: function() {
      this.model.on('change', this.render, this);
      this.model.on('destroy', this.remove, this);
    },
    render: function() {
      this.$el.html(this.template(this.model.attributes));
      return this;
    },
    remove: function() {
      this.$el.remove();
    },

    editTask: function() {
      this.$el.find('.to-do-task').html(this.editTemplate(this.model.attributes));
      // var newTitle = prompt("What should the new title be?", this.model.get('title'));
      // if(!newTitle) return;
      // this.model.set('title', newTitle);
    },
    destroyTask: function () {
      this.model.destroy();
    }
  });
  return TaskView;
});
  